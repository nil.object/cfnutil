extern crate rusoto_core;
extern crate rusoto_cloudformation;

use self::rusoto_cloudformation::{CloudFormation, DescribeStacksInput, DescribeStacksError};

#[derive(Debug)]
pub enum StackStatus {
    CreateComplete,
    CreateInProgress,
    CreateFailed,
    DeleteComplete,
    DeleteFailed,
    DeleteInProgress,
    ReviewInProgress,
    RollbackComplete,
    RollbackFailed,
    RollbackInProgress,
    UpdateComplete,
    UpdateInProgress,
    UpdateCompleteCleanupInProgress,
    UpdateRollbackComplete,
    UpdateRollbackFailed,
    UpdateRollbackInProgress,
    Unknown { status: Result<String, DescribeStacksError>},
}

impl StackStatus {
    pub fn parse(status: &String) -> StackStatus
    {
        return match status.as_ref() {
            "CREATE_COMPLETE" => StackStatus::CreateComplete,
            "CREATE_IN_PROGRESS" => StackStatus::CreateInProgress,
            "CREATE_FAILED" => StackStatus::CreateFailed,
            "DELETE_COMPLETE" => StackStatus::DeleteComplete,
            "DELETE_FAILED" => StackStatus::DeleteFailed,
            "DELETE_IN_PROGRESS" => StackStatus::DeleteInProgress,
            "REVIEW_IN_PROGRESS" => StackStatus::ReviewInProgress,
            "ROLLBACK_COMPLETE" => StackStatus::RollbackComplete,
            "ROLLBACK_FAILED" => StackStatus::RollbackFailed,
            "ROLLBACK_IN_PROGRESS" => StackStatus::RollbackInProgress,
            "UPDATE_COMPLETE" => StackStatus::UpdateComplete,
            "UPDATE_IN_PROGRESS" => StackStatus::UpdateInProgress,
            "UPDATE_COMPLETE_CLEANUP_IN_PROGRESS" => StackStatus::UpdateCompleteCleanupInProgress,
            "UPDATE_ROLLBACK_COMPLETE" => StackStatus::UpdateRollbackComplete,
            "UPDATE_ROLLBACK_FAILED" => StackStatus::UpdateRollbackFailed,
            "UPDATE_ROLLBACK_IN_PROGRESS" => StackStatus::UpdateRollbackInProgress,
            _ => StackStatus::Unknown { status: Ok(status.to_owned()) }
        }
    }

    pub fn get(client: &CloudFormation, stack_name: &str) -> Option<StackStatus> {
        let describe_stacks_input: DescribeStacksInput = DescribeStacksInput {
            stack_name: Some(stack_name.to_owned()),
            next_token: None,
        };

        return match client.describe_stacks(&describe_stacks_input).sync() {
            Ok(output) => {
                match output.stacks {
                    Some(stacks) => {
                        match stacks.iter().find(|s| s.stack_name == stack_name) {
                            Some(stack) => Some(StackStatus::parse(&stack.stack_status)),
                            None => None,
                        }
                    },
                    None => None,
                }
            },
            Err(error) => Some(StackStatus::Unknown { status: Err(error) }),
        }
    }
}