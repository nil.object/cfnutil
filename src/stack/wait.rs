extern crate rusoto_core;
extern crate rusoto_cloudformation;
extern crate chrono;

use self::rusoto_cloudformation::{CloudFormation, DescribeStackEventsInput, StackEvent};

use self::chrono::{Utc};

use super::status::{StackStatus};
use std::{process, thread, time};

pub fn ready(client: &CloudFormation, stack_name: &str) {
    let mut error_count: isize = 0;
    let mut latest_event_timestamp: String = Utc::now().format("%Y-%m-%dT%H:%M:%S.%.3fZ").to_string();

    loop {
        match StackStatus::get(client, stack_name) {
            Some(status) => {
                match status {
                    StackStatus::CreateComplete |
                    StackStatus::RollbackComplete |
                    StackStatus::UpdateComplete |
                    StackStatus::UpdateRollbackComplete => {
                        println!("Stack is ready.");
                        return;
                    }
                    StackStatus::Unknown { status } => {
                        match status {
                            Err(message) => {
                                println!("Error getting stack status: {:?}", message);
                                error_count += 1;
                                if error_count == 5 {
                                    println!("Too many errors.");
                                    process::exit(-1);
                                }
                            }
                            _ => {}
                        }
                    }
                    _ => {}
                }
            }
            None => {
                println!("Stack not found.");
                process::exit(-1);
            }
        }

        latest_event_timestamp = print_events(client, stack_name, latest_event_timestamp);
        
        thread::sleep(time::Duration::from_secs(5));
    }
}

fn print_events(client: &CloudFormation, stack_name: &str, last_event_timestamp: String) -> String {
    let input: DescribeStackEventsInput = DescribeStackEventsInput {
        next_token: None,
        stack_name: Some(stack_name.to_owned()),
    };

    match client.describe_stack_events(&input).sync() {
        Ok(result) => {
            match result.stack_events {
                Some(stack_events) => {
                    // Events are returned in reverse chronological order.
                    // Skip any events that happened before the timestamp passed.
                    let events: Vec<&StackEvent> = stack_events.iter().rev().skip_while(|e| e.timestamp < last_event_timestamp ).collect();

                    for event in events.iter() {
                        println!("{0} {1} {2} -- {3}", 
                            event.timestamp, 
                            event.logical_resource_id.to_owned().unwrap_or_default(), 
                            event.resource_type.to_owned().unwrap_or_default(), 
                            event.resource_status.to_owned().unwrap_or_default());
                        if let Some(ref status_reason) = event.resource_status_reason {
                            println!("{}", status_reason);
                        }
                    }
                    
                    return match events.len() > 0 {
                        true => events[0].timestamp.to_owned(),
                        false => last_event_timestamp,
                    };
                }
                None => {}
            }
        },
        Err(_message) => {}
    };

    return last_event_timestamp;
}